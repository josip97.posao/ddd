#ifndef HEADE_H
#define HEADER_H

#include "datatype.h"

char* zauzimanjeZn(char*, int);							//dinamicko zauzimanje za polje znakova --gotovo
char* popunjavanjeZn(char*, int);
char** zauzimanje(char**, int);							//dinamicko zauzimanje m*m memorije  --gotovo
int izbornik(char*, char**, char**);	//glavni izbornik
void pravila();											//ispis pravila	--gotovo
int novaIgra(char**, char**, int);	//nova igra --gotovo   
void ispisMemory();										//ispis naslova --gotovo
char rndZnak(char*);									//generiranje random znaka --gotovo
char* mjesanje(char*, int);								//mjesanje polja znakova  --gotovo
char** popunjavanjeSkrivene(char**, int);				//popunjavanje polja sa znakom '@'  --gotovo
char** popunjavanje(char*, char**);						//popunjavanje polja sa znakovima  --gotovo
int provjera(char**, char**, int, int, int, int);		//provjera jednakostu vrijednosti u poljima --gotovo
void ispis(char**, int);								//ispis igrace ploce  --gotovo
void unosPodataka(float, int);							//unos podataka nakog odigranog + upisivanje u file --gotovo
void ispisSvih(const IGRAC* const);						//ispis svih odigranih igara
void* pretrazivanjeTopTri();							//ispis top 3 igraca
int izlaz(char*, char**, char**);						//izlaz iz programa --gotovo

#endif
